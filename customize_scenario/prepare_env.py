# adding install psutil
# adding verify clang is installed

import sys
import subprocess


def verify_clang_is_installed(fail_allowed=True):
    subprocess.check_call(["clang", "-v"])


def install_psutil_with_pip(psutil_version):
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", f"psutil=={psutil_version}"]
    )
