import subprocess
import sys


def install_conan_with_pip(conan_version):
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", f"conan=={conan_version}"]
    )
