import os


def check_disk_space():
    try:
        df_output = os.statvfs("/")
        block_size = df_output.f_frsize
        available_space_bytes = block_size * df_output.f_bavail
        available_space = available_space_bytes / (1024**3)
        print(f"AVAILABLE SPACE {available_space} Gb")
    except Exception as e:
        raise Exception(f"Error checking disk space: {str(e)}")
