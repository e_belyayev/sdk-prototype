import subprocess

from .space import check_disk_space
from ..python_utils.python import check_python_version


def get_kernel_info():
    subprocess.check_call(["uname", "-a"])


def print_system_info():
    check_disk_space()
    get_kernel_info()
    check_python_version()
