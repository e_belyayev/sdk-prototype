from platform import python_version


def check_python_version(min_python_version="3.10"):
    print(
        f"CURRENT PYTHON VERSION is {python_version()}, {min_python_version} is required"
    )
