# prepare_env.py

# Verify python version
# Check disk space
# install conan
import sys


import json
import importlib

sys.path.append("..")


class PrepareEnvScenario:
    def __init__(self, config_file):
        self.config_file = config_file

    def execute(self):
        with open(self.config_file, "r") as config_file:
            config = json.load(config_file)

        for step in config.get("steps", []):
            method_name = step.get("method")
            if method_name:
                method_parts = method_name.split(".")
                module_name = ".".join(method_parts[:-1])
                function_name = method_parts[-1]
                try:
                    module = importlib.import_module(module_name)
                    function = getattr(module, function_name)
                    print(f">>>>>>>>>>>>>>>>>>> Executing {step.get('name')}")
                    function(**step.get("args", {}))
                except Exception as e:
                    print(f"Error executing step '{step.get('name')}': {str(e)}")
            else:
                print(f"Method not defined for step '{step.get('name')}'.")


if __name__ == "__main__":
    config_file = "../customize_scenario/prepare_env.json"
    scenario = PrepareEnvScenario(config_file)
    scenario.execute()
